function check()
{
    /*
    To check whether a given point lies inside a triangle or not.
    Here A(x1,y1), B(x2,y2), C(x3,y3) are vertices of triangle.
         P(x,y) is a point. 
    */
    let t11: HTMLInputElement=<HTMLInputElement> document.getElementById("t11");
    let t12: HTMLInputElement=<HTMLInputElement> document.getElementById("t12");
    let t21: HTMLInputElement=<HTMLInputElement> document.getElementById("t21");
    let t22: HTMLInputElement=<HTMLInputElement> document.getElementById("t22");
    let t31: HTMLInputElement=<HTMLInputElement> document.getElementById("t31");
    let t32: HTMLInputElement=<HTMLInputElement> document.getElementById("t32");
    let p1: HTMLInputElement=<HTMLInputElement> document.getElementById("p1");
    let p2: HTMLInputElement=<HTMLInputElement> document.getElementById("p2");
    let ans: HTMLInputElement=<HTMLInputElement> document.getElementById("ans");

    var x1: number=parseFloat(t11.value);
    var y1: number=parseFloat(t12.value);
    var x2: number=parseFloat(t21.value);
    var y2: number=parseFloat(t22.value);
    var x3: number=parseFloat(t31.value);
    var y3: number=parseFloat(t32.value);
    var x: number=parseFloat(p1.value);
    var y: number=parseFloat(p2.value);

    var A = area (x1, y1, x2, y2, x3, y3);  //calculating area of triangle ABC by calling function
    var A1 = area (x, y, x2, y2, x3, y3);   //calculating area of triangle PAB by calling function
    var A2 = area (x1, y1, x, y, x3, y3);   //calculating area of triangle PBC by calling function
    var A3 = area (x1, y1, x2, y2, x, y);   //calculating area of triangle PAC by calling function
    console.log(A);
    console.log(A1+A2+A3);
    if(A == A1+A2+A3)   //check if sum of A1, A2 & A3 is same as A.
    {
        ans.value="Answer : Given point lies inside the triangle";
    }
    else{
        ans.value="Answer : Given point lies outside the triangle";
    }

}
let area = function(x1:number, y1:number, x2:number, y2:number, x3:number, y3:number):number
{
    return(Math.abs((x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2))
}